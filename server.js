var express = require("express");
var app = express();
var path = require("path");
var bodyParser = require("body-parser");
var morgan = require("morgan");
var mongoose = require("mongoose");
var User = require("./app/models/user");
var PORT = process.env.PORT || 8080;
const db = mongoose.connection;

// connect db
const DB_URI =
    "mongodb+srv://venus:1234567890@cluster0-tz1sv.gcp.mongodb.net/demo-authen?retryWrites=true&w=majority";
mongoose.Promise = global.Promise;
mongoose
    .connect(DB_URI, { useNewUrlParser: true })
    .then(() => console.log("DB Connected!"));
db.on("error", err => {
    console.log("DB connection error:", err.message);
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(function(req, res, next) {
    res.setHeader("Access-Control-Allown-Origin", "*");
    res.setHeader("Access-Control-Allown-Methods", "GET,POST");
    res.setHeader(
        "Access-Control-Allown-Headers",
        "X-Requested-With, content-type, Authorization"
    );
    next();
});

app.use(morgan("dev"));
app.get("/", function(req, res) {
    res.send("Welcome to home page!");
});

var apiRouter = express.Router();
apiRouter.use(function(req, res, next) {
    console.log("Bao nhieu API router co nguoi vao trang MAIN!");
    next();
});
apiRouter.get("/", function(req, res) {
    res.json({ message: "Vi du dau tien ve api" });
});

apiRouter
    .route("/users")
    .post(function(req, res) {
        var user = new User();
        user.name = req.body.name;
        user.username = req.body.username;
        user.password = req.body.password;
        user.save(function(err) {
            if (err) {
                if (err.code == 11000)
                    return res.json({
                        success: false,
                        message: "A user with that username already exists!",
                    });
                else return res.send(err);
            }
            res.json({ message: "User created!" });
        });
    })
    .get(function(req, res) {
        User.find(function(err, users) {
            if (err) return res.send(err);
            res.json(users);
        });
    });

apiRouter
    .route("/users/:user_id")
    .get(function(req, res) {
        User.findById(req.params.user_id, function(err, user) {
            if (err) return res.send(err);
            return res.json(user);
        });
    })
    .put(function(req, res) {
        User.findById(req.params.user_id, function(err, user) {
            if (err) return res.send(err);
            if (req.body.name) user.name = req.body.name;
            if (req.body.username) user.username = req.body.username;
            if (req.body.password) user.password = req.body.password;
            user.save(function(err) {
                if (err) return res.send(err);
                res.json({ message: "User updated!" });
            });
        });
    })
    .delete(function(req, res) {
        User.remove(
            {
                _id: req.params.user_id,
            },
            function(err, user) {
                if (err) return res.send(err);
                res.json({ message: "Successfully delete!" });
            }
        );
    });

app.use("/api", apiRouter);
app.listen(PORT);
console.log("Visit me at http://localhost:" + PORT);
